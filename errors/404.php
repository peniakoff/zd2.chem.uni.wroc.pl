<!DOCTYPE html>
<html lang="en">
<head>
    <title>error 404</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <base href="http://zd2.chem.uni.wroc.pl/">
    <style>
    @import url('https://fonts.googleapis.com/css?family=Press+Start+2P');

        body {
            background-color: black;
            margin: 0;
            height: 100vh;
            font-family: 'Press Start 2P', cursive;
            min-height: 745px;
        }

        div.main {
            height: 100%;
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
        }

        div.element {
            min-width: 600px;
            display: block;
            margin: auto;
            position: relative;
        }

        div.element div.info {
            padding: 0;
            margin: 0;
            position: absolute;
            background: transparent;
            color: #D50000;
            left: 0;
            width: 100%;
            min-width: 600px;
            z-index: 10;
            text-align: center;
        }

        @media screen and (max-width: 699px) {
            div.element div.info {
                font-size: 30px;
                top: -100px;
            }
        }

        @media screen and (min-width: 700px) {
            div.element div.info {
                font-size: 36px;
                top: -112px;
            }
        }

        @media screen and (min-width: 800px) {
            div.element div.info {
                font-size: 40px;
                top: -120px;
            }
        }

        div.element div.info span {
            display: block;
            padding: 10px 0;
        }

        div.element div.lcd {
            width: 16%;
            height: 39%;
            background-color: #D50000;
            position: absolute;
            right: 1%;
            bottom: 27%;
            z-index: 10;
            animation: blinking 1s infinite;
            perspective: 100px;
            perspective-origin: right;
        }

        div.element div.lcd div {
            padding: 10px;
            position: absolute;
            transform: rotateY(-20deg);
            font-size: 28px;
            top: 40%;
            left: 15%;
            width: 40%;
        }

        @keyframes blinking {
            0% {background-color: #212121;}
            50% {background-color: #D50000;}
            100% {background-color: #212121;}
        }

        img {
            position: relative;
            width: 100%;
            height: auto;
            z-index: 15;
        }

        footer {
            position: fixed;
            bottom: 0;
            background-color: #D50000;
            margin: 0;
            padding: 0 20px;
            color: black;
            width: 100%;
            height: 20px;
            line-height: 20px;
            font-size: 12px;
            text-align: right;
            box-sizing: border-box;
            z-index: 20;
        }
    </style>
</head>
<body>
    <div class="main">
        <div class="element">
            <div class="info">
                <span>It's not game over.</span>
                <span>It's just an error.</span>
            </div>
            <div class="lcd">
                <div>
                    404
                </div>
            </div>
            <img src="errors/take-it-easy-pix.png" alt="It's just an error.">
        </div>
    </div>
    <footer>
        Error 404. Page created by Tomasz Miller
    </footer>
</body>
</html>
