<?php

include("includes/header.php");

include("includes/navbar.php");

$filesList = new ContentLoader("files", "pliki");

?>

<div class="w3-container w3-theme-d2 w3-margin-bottom">
	<h1><i class="material-icons">&#xE2C4;</i> Pliki</h1>
</div>

<div id="files" class="w3-container">

    <?php echo $filesList->filesList(); ?>

</div>

<?php include("includes/footer.php"); ?>
