<?php

include("includes/header.php");

include("includes/navbar.php");

$filesList = new ContentLoader("chemistry", "files");
$laboratoryTasks = new ContentLoader("chemistry", "laboratory_tasks");
$usefulFiles = new ContentLoader("chemistry", "useful_files");


?>

<div class="w3-container w3-theme-d2 w3-margin-bottom">
    <h1>Chemistry</h1>
</div>

<div id="files" class="w3-container">

    <?php echo $filesList->filesList(); ?>
    
</div>

<div class="w3-row-padding w3-padding-large">
    <div class="w3-col m12">
        <div class="w3-card-2 w3-hover-shadow">
            <header class="w3-container w3-theme" onclick="accordion('instruction1')" style="cursor: pointer;">
              <h3>Instructions for laboratory tasks</h3>
            </header>
            <div id="instruction1" class="w3-container w3-hide" style="padding-bottom: 16px; text-align: justify;">

                <?php echo $laboratoryTasks->ulList(); ?>
                
            </div>
        </div>
    </div>
    <div class="w3-col m12 w3-margin-top">
        <div class="w3-card-2 w3-hover-shadow">
            <header class="w3-container w3-theme" onclick="accordion('files1')" style="cursor: pointer;">
              <h3>Various useful files</h3>
            </header>
            <div id="files1" class="w3-container w3-hide" style="padding-bottom: 16px; text-align: justify;">
                
                <?php echo $usefulFiles->ulList(); ?>
                
            </div>
        </div>
    </div>
</div>

<?php include("includes/footer.php"); ?>
