<?php

include("includes/header.php");
include("includes/navbar.php");

$filesList = new ContentLoader("files", "msos");

?>

<div class="w3-container w3-theme-d2 w3-margin-bottom">
    <h1>MSOŚ</h1>
</div>

<div id="files" class="w3-container">

    <?php echo $filesList->filesList(); ?>
    
</div>

<?php include("includes/footer.php"); ?>
