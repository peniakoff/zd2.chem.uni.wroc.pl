﻿<?php

include 'includes/header.php';
include 'includes/navbar.php';

?>

<div class="w3-card-4 w3-margin w3-section">
    <header class="w3-container w3-theme-d2">
        <h1>Zakład Chemii Analitycznej Wydziału Chemii ZD-2</h1>
        <h3>The Analytical Chemistry Group in the Faculty of Chemistry</h3>
    </header>
    <div class="w3-container w3-padding-12 w3-center">
        <img src="images/Lab1.jpg" class="w3-border w3-padding-small w3-padding-8 w3-image w3-grayscale-min w3-margin home" alt="Lab1" />
        <img src="images/Lab3.jpg" class="w3-border w3-padding-small w3-padding-8 w3-image w3-grayscale-min w3-margin home" alt="Lab3" />
    </div>
</div>

<?php

include 'includes/footer.php';

?>
