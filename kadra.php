<?php

include("includes/header.php");

include("includes/navbar.php");

$personList = new ContentLoader("personel", "personel");

?>

<div class="w3-container w3-theme-d2 w3-margin-bottom">
    <h1><i class="material-icons">&#xE7EF;</i> Kadra dydaktyczna</h1>
</div>

<div class="w3-container w3-padding-12 w3-padding-xlarge w3-margin-bottom">

    <?php echo $personList->personList(); ?>

</div>

<?php include("includes/footer.php"); ?>
