        </div>
        <footer id="myFooter" class="w3-theme w3-small">
            <div class="w3-container w3-padding-16">
                <div class="w3-margin-0">Zakład Chemii Analitycznej ZD-2</div>
                <div class="w3-margin-0">Wydział Chemii Uniwersytetu Wrocławskiego</div>
                <div class="w3-margin-0">ul. F. Joliot-Curie 14</div>
                <div>50-383 Wrocław</div>
            </div>
            <div class="w3-container w3-theme-d2">
                <p>Created by <a href="mailto:info@tomaszmiller.pl">Tomasz Miller</a> with the power of <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a>, <a href="https://material.io/icons/">Material icons</a> and <a href="http://php.net/">PHP</a> | Wroclaw 2017</p>
            </div>
        </footer>
        </div>

        <script src="js/scripts.js"></script>

    </body>
</html>
