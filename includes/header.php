<?php

spl_autoload_register(function ($class) {
    include 'models/' . $class . '.php';
});

?>

<!DOCTYPE html>
<html lang="pl">
    <head>
        <title>Zakład Chemii Analitycznej Wydziału Chemii</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="theme-color" content="#008578">
        <meta name="description" content="Oficjalna strona Zakładu Chemii Analitycznej Wydziału Chemii Uniwersytetu Wrocławskiego">
        <meta name="keywords" content="zakład, chemii, analitycznej, Wydział Chemii, UWr, chemia, analityka, analityczna">
        <meta name="author" content="Tomasz Miller">
        <base href="http://zd2.chem.uni.wroc.pl/">
        <link rel="icon" sizes="192x192" href="favicon.png">
        <link rel="stylesheet" href="css/w3.min.css">
        <link rel="stylesheet" href="css/style.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    </head>
    <body>
