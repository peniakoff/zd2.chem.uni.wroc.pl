<?php

$topMenu = new ContentLoader("navigation", "top");
$leftMenu = new ContentLoader("navigation", "left");

?>

<div class="w3-top">
    <div id="navbar" class="w3-bar w3-theme w3-top w3-large w3-card-2">
        <a class="w3-bar-item w3-button w3-opennav w3-right w3-hide-large w3-large menu" href="javascript:void(0)" onclick="w3_open()"><i class="material-icons">&#xE5D2;</i></a>
        <a href="/" class="w3-bar-item w3-button title">
            Zakład Chemii Analitycznej Wydziału Chemii ZD-2
        </a>
		<div class="w3-right">

		<?php echo $topMenu->menuLoader(); ?>

		</div>
    </div>
</div>

<nav class="w3-sidenav w3-collapse w3-animate-left w3-theme-l5 w3-center w3-card-4" id="mySidenav">

<?php echo $leftMenu->menuLoader(); ?>

</nav>

<div id="myOverlay" class="w3-overlay w3-hide-large" onclick="w3_close()" title="zamknij menu" style="cursor: pointer;"></div>

<!-- main div with the content -->
<div class="w3-main" style="margin-left:250px">
    <div id="main">
