var document;

var mySidenav = document.getElementById("mySidenav");
var overlayBg = document.getElementById("myOverlay");

function w3_open() {
    'use strict';
    if (mySidenav.style.display === 'block') {
        mySidenav.style.display = 'none';
        overlayBg.style.display = "none";
    } else {
        mySidenav.style.display = 'block';
        overlayBg.style.display = "block";
    }
}

function w3_close() {
    'use strict';
    mySidenav.style.display = "none";
    overlayBg.style.display = "none";
}

function accordion(id) {
    'use strict';
    var x = document.getElementById(id);
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else { 
        x.className = x.className.replace(" w3-show", "");
    }
}

(function customizeTitle() {
    'use strict';
    var title = document.getElementsByClassName("w3-bar-item w3-button title")[0];
    if (document.documentElement.clientWidth < 320) {
        title.innerHTML = "Zakład Chemii Analitycznej Wydziału Chemii";
    } else {
        title.innerHTML = "Zakład Chemii Analitycznej Wydziału Chemii ZD-2";
    }
}());
