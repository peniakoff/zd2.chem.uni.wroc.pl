<?php

include("includes/header.php");

include("includes/navbar.php");

?>

<div class="w3-container w3-theme-d2 w3-margin-bottom">
    <h1><i class="material-icons">&#xE242;</i> Regulamin pracowni studenckiej</h1>
</div>

<div class="w3-container w3-padding-12 w3-padding-large">
    <ul id="terms" class="w3-ul w3-border w3-justify w3-padding-medium">
        <li>
            <h4>W czasie odbywania zajęć praktycznych w pracowni Chemii Analitycznej Uniwersytetu Wrocławskiego studenci zobowiązani są do przestrzegania przepisów BHP oraz o ochronie przeciwpożarowej a w szczególności:</h4>
        </li>
        <li>
            1. Przestrzegać instrukcje ppoż., regulaminy wewnętrzne – obowiązujące w pracowni, oraz polecenia osób prowadzących zajęcia dydaktyczne.
        </li>
        <li>
            2. Zabrania się przebywania w Pracowni bez opieki prowadzącego zajęcia laboratoryjne.
        </li>
        <li>
            3. Przed rozpoczęciem pracy należy zapoznać się z lokalizacją, oznakowaniem urządzeń i środków ochrony oraz charakterystyką odczynników.
        </li>
        <li>
            4. Po laboratorium należy poruszać się spokojnie i nie zagradzać ciągów komunikacyjnych. Obowiązuje bezwzględny zakaz używania krzyku oraz biegania.
        </li>
        <li>
            5. Nie wnosić do pracowni wierzchnich okryć, toreb, plecaków itp. (rzeczy te proszę zostawiać w zamkniętych szafkach obok pracowni).
        </li>
        <li>
            6. Podczas przebywania na Pracowni należy bezwzględnie stosować odzież ochronną (fartuchy z włókien naturalnych) i sprzęt ochrony osobistej (okulary ochronne, rękawiczki) zgodnie z zaleceniami osoby prowadzącej zajęcia dydaktyczne.
        </li>
        <li>
            7. Nie używać w czasie zajęć telefonów komórkowych, urządzeń odtwarzających muzykę nawet z użyciem słuchawek.
        </li>
        <li>
            8. W celach bezpieczeństwa osobistego w miarę możliwości:
                <div class="w3-margin-left w3-padding-4">a) nie używać szkieł kontaktowych tylko okulary korekcyjne</div>
                <div class="w3-margin-left w3-padding-4">b) związywać długie włosy</div>
                <div class="w3-margin-left w3-padding-4">c) stosować wygodne obuwie, ale nieodkryte i nie na wysokim obcasie</div>
                <div class="w3-margin-left w3-padding-4">d) w przypadku używania przyrządów laboratoryjnych wymagających kontaktu ze skórą np. mikroskopu, zawsze wytrzeć okular</div>
                <div class="w3-margin-left w3-padding-4">e) nie stawać na krzesłach laboratoryjnych – korzystać w tym celu z specjalnie wykonanych podwyższeń</div>
                <div class="w3-margin-left w3-padding-4">f) nie zaglądać do otworów kolb, czy pojemników na odczynniki, nie używane pojemniki zamykać</div>
                <div class="w3-margin-left w3-padding-4">g) nigdy nie przestawiać, nie przesypywać odczynników do innych nie oznakowanych pojemników, nie stosować substancji z nie oznakowanych opakowań</div>
                <div class="w3-margin-left w3-padding-4">h) zabrania się napełniania pipet ustami oraz organoleptycznej analizy odczynnika</div>
                <div class="w3-margin-left w3-padding-4">i) określone czynności laboratoryjne według poleceń osób prowadzących zajęcia  wykonywać na stojąco</div>
                <div class="w3-margin-left w3-padding-4">j) w razie chwilowej niedyspozycji powiadomić osobę prowadzącą zajęcia o niemożliwości wykonywania pewnych czynności laboratoryjnych.</div>
        </li>
        <li>
            9.	Utrzymywać powierzone stanowisko pracy w należytym porządku i czystości (przynieść własne ściereczki, ręczniki i mydło).
        </li>
        <li>
            10.	Na pracowni obowiązuje bezwzględny zakaz spożywania posiłków, picia napojów, żucia gumy, palenia papierosów, stosowania kosmetyków, przechowywania produktów żywnościowych i kosmetyków w szafkach laboratoryjnych. Poza laboratorium nie powinno się spożywać posiłków w fartuchach laboratoryjnych.
        </li>
        <li>
            11.	Nie ruszać aparatów, urządzeń, szkła laboratoryjnego, odczynników itp., nieprzewidzianych programem ćwiczeń:
                <div class="w3-margin-left w3-padding-4">- z odczynników do analiz: mikrokrystalicznej i kroplowej korzystać tylko w czasie wykonywania tych analiz</div>
                <div class="w3-margin-left w3-padding-4">- używane odczynniki odstawiać na miejsce</div>
                <div class="w3-margin-left w3-padding-4">- nie wkładać pipet do butelek z odczynnikami</div>
                <div class="w3-margin-left w3-padding-4">- nie kłaść korków od butelek na stołach.</div>
        </li>
        <li>
            12.	Zgłaszać osobie prowadzącej zajęcia fakt opuszczania pracowni na czas przerwy i po zakończeniu ćwiczeń.
        </li>
        <li>
            13.	Zachować szczególną ostrożność podczas manipulacji substancjami szkodliwymi dla zdrowia, oraz w czasie pracy przy aparaturze wytwarzającej takie substancje. Zgłosić ten fakt osobie prowadzącej zajęcia.
        </li>
        <li>
            14.	Zachować szczególną ostrożność w czasie manipulowania opakowaniami szklanymi, zwłaszcza przy ich otwieraniu i zamykaniu. O uzupełnianie stężonymi roztworami naczyń pod wyciągiem należy poprosić obsługę pracowni.
        </li>
        <li>
            15.	Właściwie użytkować sprzęt oraz urządzenia udostępnione do przeprowadzenia badań.
        </li>
        <li>
            16.	Substancje łatwo zapalne należy przechowywać w szczelnie zamkniętych opakowaniach, ustawiać z dala od źródeł ognia, zabezpieczać przed działaniem źródeł cieplnych oraz przed możliwością uszkodzeń mechanicznych.
        </li>
        <li>
            17.	Doświadczenia należy prowadzić w taki sposób, aby nie narażać osób przebywających w pobliżu na niebezpieczeństwo. Podczas manipulacji substancjami grożącymi rozpryskiem, wybuchem lub poparzeniem należy obowiązkowo używać rękawice i okulary ochronne oraz powiadomić osobę prowadzącą zajęcia.
        </li>
        <li>
            18.	Prace z substancjami lotnymi, szkodliwymi dla zdrowia należy wykonywać pod wyciągiem. Szkło laboratoryjne po zakończeniu tych prac należy myć pod wyciągiem (zlewki rozpuszczalników organicznych należy wlewać do specjalnych pojemników pod wyciągiem a zlewki metali ciężkich i zlewki jonów srebra do pojemników ustawionych na pracowni).
        </li>
        <li>
            19.	Wszystkie czynności pod wyciągiem wykonywać przy opuszczonej szybie i na stojąco.
        </li>
        <li>
            20.	O zaistnieniu w czasie ćwiczeń awarii niezwłocznie zawiadomić osobę prowadzącą zajęcia dydaktyczne.
        </li>
        <li>
            21.	Każdy zaistniały wypadek (skaleczenie, poparzenie itp.) należy niezwłocznie zgłosić osobie prowadzącej zajęcia w celu uzyskania niezbędnej pomocy.
        </li>
        <li>
            22.	W przypadku zaistnienia przerwy w dopływie wody, gazu lub prądu elektrycznego należy zakręcić odpowiednie kurki i wyłączyć urządzenia (przyrządy) znajdujące się uprzednio pod napięciem.
        </li>
        <li>
            23.	W przypadku zaistnienia pożaru należy:
            <div class="w3-margin-left w3-padding-4">- powiadomić znajdujące się w pobliżu osoby i osobę prowadzącą zajęcia</div>
            <div class="w3-margin-left w3-padding-4">- upewnić się czy zaalarmowano jednostkę straży pożarnej</div>
            <div class="w3-margin-left w3-padding-4">- usunąć materiały łatwo zapalne (zachować ostrożność)</div>
            <div class="w3-margin-left w3-padding-4">- odłączyć dopływ gazu i energii elektrycznej</div>
            <div class="w3-margin-left w3-padding-4">- gdy zaistnieje konieczność opuścić pomieszczenie, bez paniki, wyjściami głównymi lub awaryjnymi</div>
        </li>
        <li>
            24. Po zakończeniu zajęć należy zwrócić powierzone materiały i sprzęt, oraz doprowadzić do estetycznego wyglądu miejsca pracy.
        </li>
        <li>
            25. Przed opuszczeniem pracowni należy:
            <div class="w3-margin-left w3-padding-4">- wyłączyć aparaty lub urządzenia</div>
            <div class="w3-margin-left w3-padding-4">- odłączyć dopływ wody i gazu w używanych instalacjach</div>
            <div class="w3-margin-left w3-padding-4">- właściwie zabezpieczyć substancje szkodliwe dla zdrowia, oraz substancje pożarowo niebezpieczne</div>
            <div class="w3-margin-left w3-padding-4">- starannie umyć ręce po zakończeniu prac z substancjami szkodliwymi dla zdrowia.</div>
        </li>
    </ul>
</div>

<?php include("includes/footer.php"); ?>
