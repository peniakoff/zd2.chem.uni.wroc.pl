<?php

include("includes/header.php");
include("includes/navbar.php");

$file = "chemia_med";

$requirements = new ContentLoader($file, "wymagania");
$schedule = new ContentLoader($file, "hormonogram");
$seminar = new ContentLoader($file, "seminarium");

?>

<div class="w3-container w3-theme-d2 w3-margin-bottom">
    <h1>Chemia Medyczna</h1>
</div>

<div class="w3-container">
    <div class="w3-card-4 w3-margin w3-section">
        <header class="w3-container w3-theme">
          <h3>Wymagania do egzaminu</h3>
        </header>
        <div class="w3-container">

            <?php echo $requirements->ulList(); ?>

        </div>
    </div>

    <div class="w3-card-4 w3-margin w3-section">
        <header class="w3-container w3-theme">
          <h3>Hormonogram ćwiczeń laboratoryjnych</h3>
        </header>
        <div class="w3-container">

            <?php echo $schedule->ulList(); ?>

        </div>
    </div>

    <div class="w3-card-4 w3-margin w3-section">
        <header class="w3-container w3-theme">
          <h3>Seminarium</h3>
        </header>
        <div class="w3-container">

            <?php echo $seminar->ulList(); ?>

        </div>
    </div>
</div>

<?php include("includes/footer.php"); ?>
