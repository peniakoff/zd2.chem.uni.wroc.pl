<?php 

include("includes/header.php");

include("includes/navbar.php");

$filesList = new ContentLoader("instructions", "instrukcje");
$analyticalList = new ContentLoader("instructions", "chemia_analityczna");
$pdwList = new ContentLoader("instructions", "pdw_1_2");

?>

<div class="w3-container w3-theme-d2 w3-margin-bottom">
    <h1><i class="material-icons">&#xE235;</i> Instrukcje</h1>
</div>

<div id="files" class="w3-container">

    <?php echo $filesList->filesList(); ?>

</div>

<div class="w3-row-padding w3-padding-large">
    <div class="w3-col m12">
        <div class="w3-card-2 w3-hover-shadow">
            <header class="w3-container w3-theme" onclick="accordion('instruction1')" style="cursor: pointer;">
              <h3>Instrukcje do ćwiczeń laboratoryjnych dla przedmiotu "Chemia analityczna" (przedmiot rdzeniowy, 1. rok kierunku Chemia, semestr letni)</h3>
            </header>
            <div id="instruction1" class="w3-container w3-hide" style="padding-bottom: 16px; text-align: justify;">
                
                <?php echo $analyticalList->ulList() ?>
                
            </div>
        </div>
    </div>
    <div class="w3-col m12 w3-margin-top">
        <div class="w3-card-2 w3-hover-shadow">
            <header class="w3-container w3-theme" onclick="accordion('instruction2')" style="cursor: pointer;">
              <h3>Instrukcje do ćwiczeń laboratoryjnych z PDW 1 i 2 ("Analiza chemiczna środowiska i materiałów" oraz "Wybrane specjalne techniki w analizie chemicznej")</h3>
            </header>
            <div id="instruction2" class="w3-container w3-hide" style="padding-bottom: 16px; text-align: justify;">
                
                <?php echo $pdwList->ulList() ?>
                
            </div>
        </div>
    </div>
</div>

<?php include("includes/footer.php"); ?>
