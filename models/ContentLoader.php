<?php

/**
 * ContentLoader
 *
 * @author Tomasz Miller
 */

class ContentLoader {

	private $file;
	private $contentOption;

	public function __construct($file, $contentOption) {
		$this->file = $file;
		$this->contentOption = $contentOption;
	}

	public function menuLoader() {
		$json = json_decode(file_get_contents("content/" . $this->file . ".json"));
		$builedMenu = "";
		$menuType = $this->contentOption;
		foreach ($json->$menuType as $menu) {
			switch ($menuType) {
				case "top":
					$builedMenu .= '<a href="' . $menu->link . '" class="w3-bar-item w3-button w3-hide-medium w3-hide-small" title="' . $menu->linkName . '">' . $menu->linkName . '</a>';
					break;
				case "left":
					$builedMenu .= '<a href="' . $menu->link . '" ' . (isset($menu->class) ? 'class="' . $menu->class . '"' : "") . ' title="' . $menu->linkName . '">' . $menu->linkName . '</a>';
					break;
			}
		}
		return $builedMenu;
	}

	public function filesList() {
		$json = json_decode(file_get_contents("content/" . $this->file . ".json"));
		$filesList = "";
		$files = $this->contentOption;
                foreach ($json->$files as $file) {
			$filesList .= '<a href="files/' . $file->path;
                        $filesList .= '" class="w3-padding-medium w3-margin w3-theme" title="' . $file->filename . '" >';
                        $filesList .=  $file->filename . ' <span>(' . $this->getFileType("files/" . $file->path) . ', ' . ($this->getFileSize("files/" . $file->path));
                        $filesList .= ' KB)</span><i class="material-icons w3-theme-d5 w3-card-2">&#xE2C4;</i></a>';
		}
                clearstatcache();
		return $filesList;
	}

	public function personList() {
		$json = json_decode(file_get_contents("content/" . $this->file . ".json"));
		$personList = '<ul id="personelList" class="w3-ul w3-card-4">';
		$persons = $this->contentOption;
		foreach ($json->$persons as $person) {
			$personList .= '<li class="w3-padding-16">';
			$personList .= '<div style="background-image: url("images/' . $person->avatar . '.png");" class="w3-left w3-circle w3-margin-right person-thumbnails"></div>';
			$personList .= '<span class="w3-xlarge"><a href="https://profile.chem.uni.wroc.pl/api/pl/profile/' . $person->page . '" target="_blank" rel="noopener">' . $person->userName . '</a></span>';
			$personList .= '<span>' . $person->title . '</span>';
			$personList .= '<span><a href="mailto:' . $person->userEmail . '">' . $person->userEmail . '</a></span>';
			$personList .= '</li>';
		}
                $personList .= '</ul>';
		return $personList;
	}

        public function ulList() {
		$json = json_decode(file_get_contents("content/" . $this->file . ".json"));
		$ulList = '<ul class="w3-ul">';
		$li = $this->contentOption;
                foreach ($json->$li as $content) {
                    if ($content->filename == "") {
                        $ulList .= '<li><i>brak informacji</i></li>';
                        break;
                    }
                    $ulList .= '<li>';
                    if (isset($content->tag)) {
                        $ulList .= '<' . $content->tag . '>' . $content->filename . '</' . $content->tag . '>';
                    } else {
                        $ulList .= '<a href="files/' . $content->path . '" title="' . $content->filename . '">' . $content->filename;
                        $ulList .= ' <span>(' . $this->getFileType("files/" . $content->path) . ', ' . ($this->getFileSize("files/" . $content->path)) . 'KB)</span></a>';
                    }
                    $ulList .= '</li>';
                }
                $ulList .= '</ul>';
                clearstatcache();
                return $ulList;
	}

        private function getFileType($path) {

            if (file_exists($path)) {
                $info = new SplFileInfo($path);
                return ($info->getExtension() != null ) ? $info->getExtension() : "file doesn't exist";
            } else {
                return "file doesn't exist";
            }

        }

        private function getFileSize($path) {

            if (file_exists($path)) {
                return number_format((filesize($path) / 1024), 0, ',', ' ');
            } else {
                return ' - ';
            }

        }

}
